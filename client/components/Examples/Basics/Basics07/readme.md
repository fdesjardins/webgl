# Loading Objects
----

## JSON

```jsx
<Basics0601 id='mario'
  model={ 'teapot' }
  color={ [1, 1, 1, 1] }
  texture={ 'stainedGlass' }
  alpha={ 1 }
/>
```


## OBJ

<Basics0601 id='sw45'
  model={ 'teapot' }
  color={ [1, 1, 1, 1] }
  texture={ 'sw45' }
  alpha={ 1 }
/>

## STL

TODO
