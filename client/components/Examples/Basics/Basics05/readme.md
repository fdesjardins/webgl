# Lighting
----

In this example, a simple point is introduced, and twgl.js is used to set up the rotating cube buffers, shaders, and matrices. Also, a `animateScene` function is used to perform updates and transformations after each render.

<Basics0501 id='example01' color={ [1, 0, 0, 1] } />

This example also displays how props can be passed into Inferno components from markdown.

Here's the code for green light:

```jsx
<Basics0501 id='example02' color={ [0, 1, 0, 1] } />
```

<Basics0501 id='example02' color={ [0, 1, 0, 1] } />

And for blue light:

```jsx
<Basics0501 id='example02' color={ [0, 0, 1, 1] } />
```

<Basics0501 id='example03' color={ [0, 0, 1, 1] } />

In the next example, we'll see how to apply textures and blending.
