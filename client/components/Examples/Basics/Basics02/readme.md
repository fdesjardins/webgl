# Two Dimensions
----
Here's an example of how we can mesh the application state with the WebGL context
and influence objects within the scene.

Try to move the square using the buttons at below.

<Controls />

<Canvas />

<p>In the next example we'll see how to set up a three-dimensional scene.<I>beer</I></p>
