# Textures and Blending
----

```jsx
<Basics0601 id='mario' color={ [1, 1, 1, 1] } texture={ 'mario' } alpha={ 1 }/>
```
<Basics0601 id='mario' color={ [1, 1, 1, 1] } texture={ 'mario' } alpha={ 1 }/>

```jsx
<Basics0601 id='stone' color={ [1, 1, 1, 1] } texture={ 'stone' } alpha={ 1 }/>
```
<Basics0601 id='stone' color={ [1, 1, 1, 1] } texture={ 'stone' } alpha={ 1 }/>

```jsx
<Basics0601 id='carbon-fiber' color={ [1, 1, 1, 1] } texture={ 'carbonFiber' } alpha={ 1 }/>
```
<Basics0601 id='carbon-fiber' color={ [1, 1, 1, 1] } texture={ 'carbonFiber' } alpha={ 1 }/>

```jsx
<Basics0601 id='stained-glass' color={ [1, 1, 1, 1] } texture={ 'stainedGlass' } alpha={ 0.75 }/>
```
<Basics0601 id='stained-glass' color={ [1, 1, 1, 1] } texture={ 'stainedGlass' } alpha={ 0.75 }/>

```jsx
<Basics0601 id='amber-glass' color={ [1, 1, 1, 1] } texture={ 'amberGlass' } alpha={ 0.75 }/>
```
<Basics0601 id='amber-glass' color={ [1, 1, 1, 1] } texture={ 'amberGlass' } alpha={ 0.75 }/>

```jsx
<Basics0601 id='security-glass' color={ [1, 1, 1, 1] } texture={ 'infinity' } alpha={ 0.9 }/>
```
<Basics0601 id='security-glass' color={ [1, 1, 1, 1] } texture={ 'infinity' } alpha={ 0.9 }/>
