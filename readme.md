# webgl

Examples, tutorials, mini-projects, ...

Built with

- Inferno Functional Components
- Baobab Store
- Express
- Webpack
- WebGL2

## Installation

```
npm i -g webpack nodemon
npm install
```

## Usage

Build the client side and watch for changes:
```
webpack -w
```

Start the express server:
```
nodemon server/server.js -w server/
```

## License

MIT © [Forrest Desjardins](https://github.com/fdesjardins)
